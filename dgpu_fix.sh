#! /bin/bash
echo "dgpu_fix v0.3 (C) 2020 Kevin H. Patterson, KHPtech"
echo "available verbs: uninstall install fix load (daemon)"
echo " "

function has_param() {
    local term="$1"
    shift
    for arg; do
        if [[ $arg == "$term" ]]; then
            return 0
        fi
    done
    return 1
}

function dgpu_fix()
{
  rm ./fixed
  echo "Setting nvram..." | tee /dev/stderr
  nvram fa4ce28d-b62f-4c99-9cc3-6815686e30f9:gpu-power-prefs=%01%00%00%00
  echo "Disabling SIP (will fail unless system is booted in Recovery mode)..." | tee /dev/stderr
  csrutil disable
  echo "Backing up AMDRadeonX3000.kext..." | tee /dev/stderr
  rsync -av --delete /System/Library/Extensions/AMDRadeonX3000.kext /dgpu_kext/
  echo "Deleting AMDRadeonX3000.kext from /System/Library/Extensions/..." | tee /dev/stderr
  rm -Rf /System/Library/Extensions/AMDRadeonX3000.kext
  touch /dgpu_fix/fixed
}

function dgpu_load()
{
  rm ./loaded
  echo "Loading AMDRadeonX3000.kext..." | tee /dev/stderr
  kextload /dgpu_kext/AMDRadeonX3000.kext
  echo "Done!" | tee /dev/stderr
  touch /dgpu_fix/loaded
}

function on_shutdown()
{
  dgpu_fix;
  exit 0
}

function on_startup()
{
  dgpu_fix;
  dgpu_load;

  tail -f /dev/null &
  wait $!
}

if has_param 'uninstall' "$@"; then
  launchctl unload -w /Library/LaunchDaemons/com.khptech.dgpu_fix.plist
  launchctl unload -w /dgpu_fix/com.khptech.dgpu_fix.plist
  rm /Library/LaunchDaemons/com.khptech.dgpu_fix.plist
fi

if has_param 'install' "$@"; then
  mkdir -p /dgpu_kext
  chown -R root:wheel /dgpu_fix/ /dgpu_kext/
  chmod -R ug+rwX,o+rX-w /dgpu_fix/
  chmod ug+rwX,o+rX-w /dgpu_kext/
  cp /dgpu_fix/com.khptech.dgpu_fix.plist /Library/LaunchDaemons/.
  chown root:wheel /Library/LaunchDaemons/com.khptech.dgpu_fix.plist
  launchctl load -w /Library/LaunchDaemons/com.khptech.dgpu_fix.plist
fi

if has_param 'daemon' "$@"; then
  trap on_shutdown SIGTERM
  trap on_shutdown SIGKILL
  on_startup;
fi

if has_param 'fix' "$@"; then
  dgpu_fix;
fi

if has_param 'load' "$@"; then
  dgpu_load;
fi
